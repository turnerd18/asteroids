// Spaceship C# Script (version: 1.02)
// SPACE UNITY - Space Scene Construction Kit
// http://www.spaceunity.com
// (c) 2013 Stefan Persson

// DESCRIPTION:
// Thruster, steering and weapon control script for Spaceship prefab.

// INSTRUCTIONS:
// This script is attached to the Spaceship demo prefab. Configure parameters to suit your needs.

// PARAMETERS:
//  thrusters			(Thruster array containing reference to thrusters prefabs attached to the ship for propulsion)
//  rollRate			(multiplier for rolling the ship when steering left/right)	
//  yawRate				(multiplier for rudder/steering the ship when steering left/right)
//  pitchRate			(multiplier for pitch when steering up/down)
//  weaponMountPoints	(Vector3 array for mount points relative to ship where weapons will fire from)
//  laserShotPrefab		(reference to Laser Shot prefab, i.e. the laser bullet prefab to be instanitated)
//  soundEffectFire		(sound effect audio clip to be played when firing weapon)

// HINTS:
// The propulsion force of the thruster is configured for each attached thruster in the Thruster script.

// Version History
// 1.02 - Prefixed with SU_Spaceship to avoid naming conflicts.
// 1.01 - Initial Release.

using UnityEngine;
using System.Collections;

public class SU_Spaceship : MonoBehaviour {
	
	// Array of thrusters attached to the spaceship
	public SU_Thruster[] thrusters;
	// Specify the roll rate (multiplier for rolling the ship when steering left/right)	
	public float rollRate = 100.0f;
	// Specify the yaw rate (multiplier for rudder/steering the ship when steering left/right)
	public float yawRate = 30.0f;
	// Specify the pitch rate (multiplier for pitch when steering up/down)
	public float pitchRate = 100.0f;
	// Laser shot sound effect
	public AudioClip soundEffectFire;
	
	// Private variables
	private Rigidbody _cacheRigidbody;
	
	
	void Start () {		
		// Ensure that the thrusters in the array have been linked properly
		foreach (SU_Thruster _thruster in thrusters) {
			if (_thruster == null) {
				Debug.LogError("Thruster array not properly configured. Attach thrusters to the game object and link them to the Thrusters array.");
			}			
		}
		
		// Cache reference to rigidbody to improve performance
		_cacheRigidbody = rigidbody;
		if (_cacheRigidbody == null) {
			Debug.LogError("Spaceship has no rigidbody - the thruster scripts will fail. Add rigidbody component to the spaceship.");
		}
	}
	
	void Update () {
		// Start all thrusters when pressing Fire 1
		if (Input.GetButtonDown("up")) {		
			foreach (SU_Thruster _thruster in thrusters) {
				_thruster.StartThruster();
			}
		}
		// Stop all thrusters when releasing Fire 1
		if (Input.GetButtonUp("up")) {		
			foreach (SU_Thruster _thruster in thrusters) {
				_thruster.StopThruster();
			}
		}
		
		if (Input.GetButtonDown("space")) {
			// Play sound effect when firing
			if (soundEffectFire != null) {
				audio.PlayOneShot(soundEffectFire);
			}
		}		
	}
}
