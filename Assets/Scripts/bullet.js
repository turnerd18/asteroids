// Inspector Variables
var bulletSpeed : float = 10.0;
var barrel : Transform;
var xmax : float = 8.25;
var xmin : float = -8.25;
var ymax : float = 5.25;
var ymin : float = -5.25;

function Update () {
	if ((transform.position.x >= xmax) || (transform.position.x <= xmin) ||
		(transform.position.y >= ymax)   || (transform.position.y <= ymin))
		Destroy(gameObject);

	transform.Translate(0, bulletSpeed * Time.deltaTime, 0, transform);
}

function OnTriggerEnter (other : Collider) {		
	if (other.gameObject.tag == "asteroid") {
		Destroy(gameObject);
		other.gameObject.GetComponent("asteroid").Explode();	
	}
}