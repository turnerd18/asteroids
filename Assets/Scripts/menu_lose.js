// Lose Menu Script
var highscoreUrl="http://dtgames.co.nf/assets/php/asteroids/display.php";
var buttonWidth : float = 100.0;
var buttonHeight : float = 50.0;

private var gm : GameManager;
private var hsName = "Anonymous";
private var isHighScore = false;
private var posted = false;

function Start() {
	gm = new GameManager();
	checkScore(gm.score);
	//checkScore(gm.score);
}

function OnGUI () {
	GUI.Label( Rect( (Screen.width/2) - (buttonWidth/2), (Screen.height/2) - buttonHeight - 60, buttonWidth, buttonHeight), "GAME OVER");
	GUI.Label( Rect( (Screen.width/2) - (buttonWidth/2), (Screen.height/2) - buttonHeight - 30, buttonWidth, buttonHeight), "Score: " + gm.score);
	
	if(GUI.Button( Rect( (Screen.width/2) - (buttonWidth/2), (Screen.height/2) - buttonHeight, buttonWidth, buttonHeight), "Back to Menu"))
		Application.LoadLevel("menu_main");
		
	var isWebPlayer = (Application.platform == RuntimePlatform.OSXWebPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer);
	if (!isWebPlayer) {
		if(GUI.Button( Rect( (Screen.width/2) - (buttonWidth/2), (Screen.height/2) + 10, buttonWidth, buttonHeight), "Exit Game"))
			Application.Quit();
	}
	
	if (isHighScore && !posted) {
		hsName = GUI.TextField (Rect (10, 10, 200, 22), hsName, 25);
		if(GUI.Button(Rect(220,10,120,22), "Post High Score!")) {
			gameObject.GetComponent("post_high_score").postScore(hsName,gm.score);
			posted = true;
		}
	}
}

function checkScore(score) {
	var hs_get = WWW(highscoreUrl);
	yield hs_get;
	if (hs_get.error)
		Debug.Log("There was an error getting the high score: " + hs_get.error);
	else {
		var hss = hs_get.text.Split("\t\n"[0]);
		var lowestHS = int.Parse(hss[hss.length - 1]);
		if (score > lowestHS)
			isHighScore = true;
	}
}