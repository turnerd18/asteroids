// Main Menu Script

var buttonWidth : float = 100.0;
var buttonHeight : float = 50.0;

function OnGUI () {
	if(GUI.Button( Rect( (Screen.width/2) - (buttonWidth/2), (Screen.height/2) - buttonHeight - 60, buttonWidth, buttonHeight), "Start Game"))
		Application.LoadLevel("level");
		
	if(GUI.Button( Rect( (Screen.width/2) - (buttonWidth/2), (Screen.height/2) - buttonHeight, buttonWidth, buttonHeight), "High Scores"))
		Application.LoadLevel("menu_high_scores");
		
	var isWebPlayer = (Application.platform == RuntimePlatform.OSXWebPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer);
	if (!isWebPlayer) {
		if(GUI.Button( Rect( (Screen.width/2) - (buttonWidth/2), (Screen.height/2) + 10, buttonWidth, buttonHeight), "Exit Game"))
			Application.Quit();
	}
}