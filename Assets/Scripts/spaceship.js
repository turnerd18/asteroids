// Player Script

// Inspector Variables
var playerSpeed : float = 5.0;
var bulletSpeed : float = 125.0;
var verMin : float = -4.8;
var verMax : float = 4.8;
var horMin : float = -7.8;
var horMax : float = 7.8;

var projectile : Transform;
var barrel : Transform;

// Private Variables
private var inputEnabled : boolean = true;

function Update () {	
	if (inputEnabled) {
		var transV : float = Input.GetAxis("Vertical") * playerSpeed * Time.deltaTime;
		transform.Translate(0, 0, transV);
		transform.Rotate(0, -Input.GetAxis("Horizontal") * bulletSpeed * Time.deltaTime, 0);
	}	
	
	// Player position is equal to 'X' number, then number equals 'X'
	transform.position.x = Mathf.Clamp(transform.position.x, horMin, horMax);
	transform.position.y = Mathf.Clamp(transform.position.y, verMin, verMax);

	// Create a bullet
	if (Input.GetKeyDown("space")) {
		Instantiate(projectile, barrel.position, barrel.rotation);
	}
}

function toggleInput() {
	if (inputEnabled)
		inputEnabled = false;
	else
		inputEnabled = true;
}