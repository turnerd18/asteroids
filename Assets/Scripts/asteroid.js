// Inspector Variables
var asteroidSpeed : float = 1.0;
var explosion : Transform;
var smallerAsteroid : Transform; 
var childrenToSpawn : int = 0; 
var pointValue : int = 100;
var xmin : float = -9.75;
var xmax : float = 9.75;
var ymin : float = -6.75;
var ymax : float = 6.75;

// Private Variables
private var direction : Vector3;
private var gm : GameManager;

function Start() {
	RandomDirection();
	gm = GameObject.Find("Game Manager").gameObject.GetComponent("game_manager").gm;
	gm.IncreaseAsteroids();
}

function Update () {
	transform.Translate(direction * asteroidSpeed * Time.deltaTime);

	// Check for the bottom of the screen
	if ((transform.position.x >= xmax) || (transform.position.x <= xmin) ||
		(transform.position.y >= ymax)   || (transform.position.y <= ymin))
		Reset();
}

function OnTriggerEnter(other : Collider) {
	if (other.gameObject.tag == "player") {
		var pos = other.transform.position;
		var rot = other.transform.rotation;
		Instantiate(explosion, pos, rot); 
		
		// Find out whether to respawn or end the game
		var GO = GameObject.Find("Game Manager"); 
		GO.GetComponent("game_manager").gm.LoseOneLife();
		if (GO.GetComponent("game_manager").gm.lives == 0) {
			Destroy(other.gameObject);	
			yield WaitForSeconds(3);
			GO.name = "Game Manager Old";
			DontDestroyOnLoad(GO);
			Application.LoadLevel("menu_lose");
		}
		else { // Respawn 
			other.gameObject.GetComponent("player").toggleInput();
			other.gameObject.SetActive(false);
			yield WaitForSeconds(2);
			other.gameObject.SetActive(true);
			other.gameObject.transform.rotation.z = 0.0;
			other.gameObject.transform.position = Vector3(0,0,0); 
			other.gameObject.GetComponent("player").toggleInput();
		}
	}
}
 
function Explode() {
	var pos = transform.position;
	var rot = transform.rotation;
	Instantiate(explosion, pos, rot);
	// spawn children if there are any 
	var waitForNew;
	if (smallerAsteroid) {
		for (var i = 0; i < childrenToSpawn; i++) {
			waitForNew = Instantiate(smallerAsteroid, pos, rot); 
		} 
	}  
	yield waitForNew; // wait for the new asteroids to be created (if any) before checking for 0
	gm.IncreaseScore(pointValue);
	gm.DecreaseAsteroids(); 
	
	// check if the player has beat the level
	if (gm.asteroids == 0) {
		GameObject.Find("Game Manager").GetComponent("game_manager").WinLevel();
	}
	Destroy(gameObject);
} 

function Reset() {
	RandomDirection();
	var integer : int = Mathf.Round(Random.value);
	
	// generate asteroid on one of the sides
	if (integer == 0) {
		transform.position.y = Random.Range(ymin,ymax); 
		integer = Mathf.Round(Random.value);
		if (integer == 0)
			transform.position.x = xmax;
		else
			transform.position.x = xmin;
	} 
	// generate asteroid at top or bottom
	else {
		transform.position.x = Random.Range(xmin,xmax);
		integer = Mathf.Round(Random.value);
		if (integer == 0)
			transform.position.y = ymax;
		else
			transform.position.y = ymin;
	}
}

function RandomDirection() { 
	var integer = Mathf.Round(Random.value); 
	if (integer == 0)
		direction.x = Random.value + 1;
	else
		direction.x = -Random.value - 1;   
		
	integer = Mathf.Round(Random.value);
	if (integer == 0)
		direction.y = Random.value + 1; 
	else 
		direction.y = -Random.value - 1; 
	direction.z = 0;
}