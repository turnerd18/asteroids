var addScoreUrl="http://dtgames.co.nf/assets/php/asteroids/addscore.php?"; //be sure to add a ? to your url
var highscoreUrl="http://dtgames.co.nf/assets/php/asteroids/display.php";    

//This connects to a server side php script that will add the name and score to a MySQL DB.
function postScore(name, score) {
    var postscoreUrl = addScoreUrl + "name=" + name + "&score=" + score;
 
    // Post the URL to the site and create a download object to get the result.
    hs_post = WWW(postscoreUrl);
    yield hs_post; // Wait until the download is done
    if(hs_post.error) {
        print("There was an error posting the high score: " + hs_post.error);
    }
}
