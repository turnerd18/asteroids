var highscoreUrl="http://dtgames.co.nf/assets/php/asteroids/display.php";    

private var highScores = "No Scores to Display";
private var isLoading = true;

function Start() {
	getScores();
}
 
// Get the scores from the MySQL DB to display in a GUIText.
function OnGUI() {
	if(GUI.Button(Rect(10,10,100,20), "Back to Menu"))
		Application.LoadLevel("menu_main");
		
	GUI.Label( Rect( (Screen.width/2) - 50, (Screen.height/2) - 105, 100, 20), "HIGH SCORES");
		
	if (isLoading) {
		GUI.Label( Rect( (Screen.width/2) - 50, (Screen.height/2) - 85, 100, 50), "Loading...");
	}
	else {
		GUI.Label( Rect( (Screen.width/2) - 50, (Screen.height/2) - 85, 150, 160), highScores);
	}
}

function getScores() {
	var hs_get = WWW(highscoreUrl);
	yield hs_get;
	if (hs_get.error)
		Debug.Log("There was an error getting the high score: " + hs_get.error);
	else {
		highScores = hs_get.text;
	}
	isLoading = false;
}