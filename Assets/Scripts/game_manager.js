public class GameManager {
	private static var gameManager : GameManager = null;
	
	var score : int = 0; 
	var level : int = 0; 
	var asteroids : int = 0;
	var lives : int = 3;

	function GameManager() {
		if (GameObject.Find("Game Manager Old")) {
			var old = GameObject.Find("Game Manager Old").GetComponent("game_manager").gm;
			score = old.score;
			level = old.level; 
			lives = old.lives;
			var GO = GameObject.Find("Game Manager Old");
			GameObject.Destroy(GO);
		}
		level++;
	}
			
	function IncreaseScore(change) {
		score += change;
	}
	
	function IncreaseAsteroids() {
		asteroids++;
	}
	
	function DecreaseAsteroids() { 
		asteroids--; 
	}
	
	function LoseOneLife () {
		lives--;
	}
}

var asteroid : Transform;

public var gm; 
 
function Start() {
	gm = new GameManager();	
	InitAsteroids(gm.level);
}
 
function OnGUI () {
	GUI.Label(Rect(10,10,100,20), "Score: " + gm.score);
	GUI.Label(Rect(10,30,100,20), "Lives: " + gm.lives);
}

function InitAsteroids(number : int) {
	for (var i = 0; i < number; i++)
		Instantiate(asteroid);
}

function WinLevel() {
	yield WaitForSeconds(2);
	var GO = GameObject.Find("Game Manager");
	GO.name = "Game Manager Old";
	DontDestroyOnLoad(GO);
	Application.LoadLevel("level");
}